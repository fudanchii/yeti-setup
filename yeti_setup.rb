execute "add yarn pkg" do
  command <<-EOC
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
  EOC
  not_if "test -f /etc/apt/sources.list.d/yarn.list"
end

package "apt-transport-https"
    
execute "apt update" do
  command "apt update -y"
end

%w(
  build-essential
  git
  python-dev
  mongodb
  redis-server
  libxml2-dev
  libxslt-dev
  zlib1g-dev
  python-virtualenv
  python-pip
  nginx
  yarn).each { |pkg| package pkg }

%w(
  mongodb
  redis-server
  nginx
  ).each { |srv| service srv }

git "/opt/yeti" do
  repository "https://github.com/yeti-platform/yeti.git"
end

execute "pip install -r requirements.txt" do
  cwd "/opt/yeti"
  command "pip install -r requirements.txt && pip install uwsgi"
end

execute "yarn install" do
  cwd "/opt/yeti"
  command "yarn"
end

user "yeti"

execute "enable services" do
  cwd "/opt/yeti"
  command <<-EOC
  cp extras/systemd/*.service /etc/systemd/system/ && \
  systemctl enable yeti_uwsgi.service && \
  systemctl enable yeti_oneshot.service && \
  systemctl enable yeti_feeds.service && \
  systemctl enable yeti_exports.service && \
  systemctl enable yeti_analytics.service && \
  systemctl enable yeti_beat.service && \
  systemctl daemon-reload && \
  chown -R yeti:yeti /opt/yeti && \
  chmod +x /opt/yeti/yeti.py
  EOC
  not_if "test -f /etc/systemd/system/yet_uwsgi.service"
end

execute "enable nginx" do
  cwd "/opt/yeti"
  command <<-EOC
  rm -f /etc/nginx/sites-enabled/default && \
  cp extras/nginx/yeti /etc/nginx/sites-available/ && \
  ln -s /etc/nginx/sites-available/yeti /etc/nginx/sites-enabled/yeti && \
  systemctl restart nginx
  EOC
  not_if "test -f /etc/nginx/sites-available/yeti"
end

execute "start yeti oneshot service" do
  command <<-EOC
  systemctl start yeti_oneshot.service && \
  sleep 5
  EOC
end

execute "start yeti services" do
  command <<-EOC
  systemctl start yeti_feeds.service && \
  systemctl start yeti_exports.service && \
  systemctl start yeti_analytics.service && \
  systemctl start yeti_beat.service && \
  systemctl start yeti_uwsgi.service
  EOC
end
